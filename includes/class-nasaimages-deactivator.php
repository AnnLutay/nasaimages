<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Nasaimages
 * @subpackage Nasaimages/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Nasaimages
 * @subpackage Nasaimages/includes
 * @author     Ann Lutay <anyalutay@gmail.com>
 */
class Nasaimages_Deactivator {

	public static function deactivate() {

	}

}
