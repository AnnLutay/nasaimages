msgid ""
msgstr ""
"Project-Id-Version: nasaimages\n"
"POT-Creation-Date: 2021-03-04 10:55+0200\n"
"PO-Revision-Date: 2021-03-04 10:55+0200\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: en\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 2.4.2\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Poedit-KeywordsList: __;_e;esc_attr_e\n"
"X-Poedit-SearchPath-0: .\n"

#: public/class-nasaimages-public.php:81
msgid "Posts NASA Gallery"
msgstr ""

#: public/class-nasaimages-public.php:82 public/class-nasaimages-public.php:92
msgid "Post NASA Gallery"
msgstr ""

#: public/class-nasaimages-public.php:83 public/class-nasaimages-public.php:84
msgid "Add NASA Gallery post"
msgstr ""

#: public/class-nasaimages-public.php:85
msgid "Edit NASA Gallery post"
msgstr ""

#: public/class-nasaimages-public.php:86
msgid "New NASA Gallery post"
msgstr ""

#: public/class-nasaimages-public.php:87
msgid "View NASA Gallery post"
msgstr ""

#: public/class-nasaimages-public.php:88
msgid "Search NASA Gallery posts"
msgstr ""

#: public/class-nasaimages-public.php:89
msgid "No NASA Gallery posts found"
msgstr ""

#: public/class-nasaimages-public.php:90
msgid "No NASA Gallery posts in trash"
msgstr ""
