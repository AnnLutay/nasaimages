<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @since             1.0.0
 * @package           Nasaimages
 *
 * @wordpress-plugin
 * Plugin Name:       NASA images
 * Version:           1.0.0
 * Author:            Anya Lutay
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       nasaimages
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 */
define( 'NASAIMAGES_VERSION', '1.0.0' );

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-nasaimages-activator.php
 */
function activate_nasaimages() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-nasaimages-activator.php';
	Nasaimages_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-nasaimages-deactivator.php
 */
function deactivate_nasaimages() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-nasaimages-deactivator.php';
	Nasaimages_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_nasaimages' );
register_deactivation_hook( __FILE__, 'deactivate_nasaimages' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-nasaimages.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_nasaimages() {

	$plugin = new Nasaimages();
	$plugin->run();

	if( ! wp_next_scheduled('nasaimages_apod_get_images') ){
		wp_schedule_event( time(), 'daily', 'nasaimages_apod_get_images' );
	}

}
run_nasaimages();
