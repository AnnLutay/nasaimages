(function( $ ) {
	'use strict';

	/**
	 * Code for public-facing JavaScript source
	 */
	$( window ).load(function() {
		$('.nasaimages_gallery').slick({
			dots: true,
			infinite: true,
			speed: 500,
		});
	})
})( jQuery );
