<?php

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Nasaimages
 * @subpackage Nasaimages/public
 * @author     Ann Lutay <anyalutay@gmail.com>
 */
class Nasaimages_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/nasaimages-public.css', array(), $this->version, 'all' );
		wp_enqueue_style( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css', array( ), '1.8.1', false );
		wp_enqueue_style( 'slick-theme', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css', array( ), '1.8.1', false );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/nasaimages-public.js', array( 'jquery', 'slick' ), $this->version, false );
		wp_enqueue_script( 'slick', '//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js', array( ), '1.8.1', false );

	}

	/**
	 * Create a new post type
	 *
	 * @since	1.0.0
	 */
	public function register_post_type()
	{
		register_post_type('post-nasa-gallery', array(
			'labels'             => array(
				'name'               => __('Posts NASA Gallery', $this->plugin_name),
				'singular_name'      => __('Post NASA Gallery', $this->plugin_name),
				'add_new'            => __('Add NASA Gallery post', $this->plugin_name),
				'add_new_item'       => __('Add NASA Gallery post', $this->plugin_name),
				'edit_item'          => __('Edit NASA Gallery post', $this->plugin_name),
				'new_item'           => __('New NASA Gallery post', $this->plugin_name),
				'view_item'          => __('View NASA Gallery post', $this->plugin_name),
				'search_items'       => __('Search NASA Gallery posts', $this->plugin_name),
				'not_found'          => __('No NASA Gallery posts found', $this->plugin_name),
				'not_found_in_trash' => __('No NASA Gallery posts in trash', $this->plugin_name),
				'parent_item_colon'  => '',
				'menu_name'          => __('Post NASA Gallery', $this->plugin_name)

			),
			'public'             => true,
			'publicly_queryable' => true,
			'show_ui'            => true,
			'show_in_menu'       => true,
			'query_var'          => true,
			'rewrite'            => true,
			'capability_type'    => 'post',
			'has_archive'        => true,
			'hierarchical'       => false,
			'menu_position'      => null,
			'supports'           => array('title','editor','author','thumbnail','excerpt','comments')
		) );
	}

	/**
	 * Shortcode handler for showing gallery
	 *
	 * @since	1.0.0
	 */
	public function gallery()
	{
		$image_posts = get_posts(array(
			'numberposts'   => 5,
			'post_type'     => 'post-nasa-gallery',
			'orderby'       => 'date',
			'order'         => 'DESC',
		));
		$images = [];
		foreach ($image_posts as $image_post){
			$this->regenerate_image($image_post->ID);
			array_push($images, array(
				'url'		=> get_the_post_thumbnail_url($image_post->ID, 'nasaimages_gallery'),
				'caption'	=> get_the_post_thumbnail_caption($image_post->ID),
			));
		}
		$_SESSION['nasaimages_gallery_images'] = $images;
		ob_start();
		include( plugin_dir_path( __FILE__ ) . 'partials/nasaimages-public-display.php');
		return ob_get_clean();
	}

	private function regenerate_image($post_id)
	{
		if(!image_get_intermediate_size($post_id, 'nasaimages_gallery')) {
			require_once(ABSPATH . 'wp-admin/includes/image.php');
			$img_id = get_post_thumbnail_id($post_id);
			wp_generate_attachment_metadata($img_id, wp_get_original_image_path($img_id));
		}
	}

}
