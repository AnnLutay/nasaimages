<?php

/**
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       http://example.com
 * @since      1.0.0
 *
 * @package    Nasaimages
 * @subpackage Nasaimages/public/partials
 */

$images = $_SESSION['nasaimages_gallery_images'];
?>

<div class='nasaimages_gallery__wrapper'>
    <div class="nasaimages_gallery">
        <?php foreach ( $images as $image ) :?>
            <div>
                <img
                    src="<?php echo $image['url']; ?>"
                    alt="<?php echo $image['caption']; ?>">
            </div>
        <?php endforeach; ?>
    </div>
</div>

<?php
unset($_SESSION['nasaimages_gallery_images']);