<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Nasaimages
 * @subpackage Nasaimages/admin
 * @author     Ann Lutay <anyalutay@gmail.com>
 */
class Nasaimages_Admin {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of this plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;

	}

	/**
	 * Register the stylesheets for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/nasaimages-admin.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the admin area.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Plugin_Name_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Plugin_Name_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/nasaimages-admin.js', array( 'jquery' ), $this->version, false );

	}

	/**
	 * Get images from NASA API
	 *
	 * @since	1.0.0
	 */
	public function apod_get_images()
	{
		$args = array(
			'post_status' 		=> 'publish',
			'post_type'			=> 'post-nasa-gallery',
			'posts_per_page'	=> 1,
			'fields'			=> 'ids'
		);
		$posts = new WP_Query( $args );
		$num = ( $posts->found_posts == 0 ) ? 5 : 1;
		$apod = wp_remote_get( 'https://api.nasa.gov/planetary/apod?api_key=hY52qSWp7U0p6JgbhZhbeil2tmJguOL3QFHDg9rO&thumbs=true&count=' . $num );

		if( wp_remote_retrieve_response_code($apod) == 200 ){
			require_once( ABSPATH . 'wp-admin' . '/includes/image.php' );
			require_once( ABSPATH . 'wp-admin' . '/includes/file.php' );
			require_once( ABSPATH . 'wp-admin' . '/includes/media.php' );
			$images = json_decode(wp_remote_retrieve_body($apod));

			foreach( $images as $image ){

				$post_id = wp_insert_post( array(
						'post_title'	=> sanitize_text_field( $image->date ),
						'post_type'		=> 'post-nasa-gallery',
						'post_status'   => 'publish',
					)
				);

				$img_url = ( isset($image->hdurl) ) ? $image->hdurl : $image->thumbnail_url;
				$image_array  = array(
					'name'     => wp_basename( $img_url ),
					'tmp_name' => download_url( $img_url )
				);

				// If error storing temporarily, return the error.
				if ( is_wp_error( $image_array['tmp_name'] ) ) {
					return $image_array['tmp_name'];
				}

				// Do the validation and storage stuff.
				$thumbnail = media_handle_sideload( $image_array, $post_id, sanitize_text_field($image->explanation) );

				// If error storing permanently, unlink.
				if ( is_wp_error( $thumbnail ) ) {
					@unlink( $image_array['tmp_name'] );
				}

				set_post_thumbnail($post_id, $thumbnail);

			}
		}
	}

	/**
	 * Set new image size
	 *
	 * @since	1.0.0
	 */
	public function add_new_image_size()
	{
		if ( function_exists( 'add_image_size' ) ) {
			add_image_size( 'nasaimages_gallery', 500, 300, true );
		}
	}

}
